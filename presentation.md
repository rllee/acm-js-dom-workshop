---
marp: true
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url("https://marp.app/assets/hero-background.svg")
---

![bg right](https://media-cdn.tripadvisor.com/media/photo-s/07/40/d4/f7/dunkin-donuts.jpg)

# **The Web runs on Javascript**
### Web Development Part 4
### W&M ACM

#### 04/13/2023

---

# **Road Map**

1. JavaScript Workshop (~6:30)
2. Card games! (~7:00)
3. Technical Interview Prep Info Session (~7:20)

---

# **Today's project**

A bookmarklet (a small program we can store in the browser's bookmark bar) to add the shipping price to the listing price on eBay. (Shop smarter!)

https://code.wm.edu/

---

# **What is JavaScript?**

* The scripting language for the web 
* Prior to JavaScript, all websites were static HTML files with CSS styles
* Each browser has their own JavaScript engine (this is part of what makes certain browsers faster than others)
* Unrelated to Java (renamed from LiveScript to JavaScript in 1995 for clout)
* Syntax is inspired by C

---

# **Running code in a web browser?**

* Press `CTRL/Cmd`+`Shift`+`I` or `F12` to open the Inspect view
* Tabs for HTML Elements on the page, styles, network requests, but our favorite is the *browser console!*
* If you have an existing webpage open, you will most likely see errors or logs from the current webpage.
* We can run JavaScript code from the browser console, from the URL bar with `javascript:`, or (as is most common) from an HTML `<script>` tag.

---

# **Hello, world wide web.**

```js
console.log("Hello, world");
```

Calls the `log` method on the `console` object.

Logs the string to the browser console.

---

# **Syntax**

```js
function guessNumber() {
    "use strict";
    let guess; // I use let because I want to reassign this
    const correctAnswer = 3280; // I use const so I can't reassign this.
    while(true) {
        guess = prompt("Guess a number:");
        if (guess == correctAnswer) {
            alert("Correct! This is the location of weekly ACM meetings");
            return;
        } else {
            alert("Try again.")
        }
    }
}
```

---

# **Objectify everything**

* JavaScript is built around objects.

```js
const student = {
    id: 931234567,
    classes: ["Calc II", "Computational Problem Solving"]
};
console.log(student.id, student["id"], student.classes[1]);
```

* Unlike Python, objects don't have to come from a class (classes weren't part of JS at all for 20 years). They are just variables that store structured data as keys and values.
* JSON = JavaScript Object Notation

---

# ~~Lists~~ **Arrays**

<div style="display: flex;justify-content:stretch;width:100%">
<div style="flex:1">

```js
const arr = [true, 1, 'word'];

console.log(arr.length, arr[arr.length - 2])

arr.push(false);

for (let item of arr.slice(1)) {
    console.log(item);
}

```

</div>
<div style="flex:1">

```python
arr = [True, 1, 'word']

print(len(arr), arr[-2])

arr.append(False)

for item in arr[1:]:
    print(item)
```

</div>
</div>

```shell
3 1
True
1
word
False
```

---

# **Lots of ways to make functions!**

```js
function name(arg1, arg2) { 
    /*code*/
    return value; 
}
const name = function(arg1, arg2) { 
    /*code*/
    return value;
};
const name = (arg1, arg2) => {
    /*code*/
    return value;
}
const name = arg1 => value;
/*example:*/ const getFirstItem = arr => arr[0];
``` 

---

# **DOM, DOM, DOM!!!**
#### (dramatic sound effect)

* The DOM is the **Document-Object Model**
* A browser API that allows JS to make changes to the HTML document.
* It's built in to every browser and this is how you can create interactive webpages.

---

![bg left 100%](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5a/DOM-model.svg/1200px-DOM-model.svg.png)

# **DOCUMENT!**

* The browser keeps track of HTML elements as a tree in the `document` object.
* Our JS code can access items in that tree in many different ways.

---

# **You've been selected.**

`tagname` or `.class` or `#id`

```js
/*selects the FIRST tag with class article-section*/
const mainSection = document.querySelector(".article-section"); 

/*selects all p tags in the first section*/
let pTagList = mainSection.querySelectorAll("p");

/* if we want to find ALL p tags in ALL article sections, we can use this query */
let pTagList = mainSection.querySelectorAll(".article-section p");
```

No matching element? The methods will return `undefined` ~~ `None`.

**You can now complete tasks 1-3 in the skeleton file.**

---

# **Don't let `undefined` define you**

```js
document.querySelector(".main-section")?.querySelector("p")?.querySelector("span")
```

If any of these calls would have been unsuccessful, the program would have crashed. However, when we chain them with `?.`, if any of them are `undefined`, the whole expression becomes `undefined`.

```js
const shippingText = document.querySelector('.shipping-info')?.innerText ?? 'Not Found';
```

The `??` stops us from having to make an if statement to check if the query is unsuccessful. We can just replace it with "Not Found" so it will still be a string. **DO TASK FOUR!**


---

# **Stay in your element**

```js
element.innerText /*Gets text inside an element as a string*/
element.innerHTML /*Gets HTML inside an element as a string*/
element.style /*Allows us to edit an element's inline styles as an object*/

//example (HINT HINT)
element.style.color = 'red';
element.style.textDecoration = 'line-through';
```

--- 

# **I can handle it!**

```js
element.onclick = function () {
    /*when you click this element, what do you want to happen*/
}
```

We have many events in JavaScript and we can set event handlers for them most easily like this!

Other events: `onkeypress`, `onhover`, `onfocus`!

**DO TASK SIX AND SEVEN**

---

# **Waving through a `window`**
Is that too theatre kid of me?

The window object represents the current active browser tab. There are many useful methods like `window.open()` which opens a new URL.

We can also use the window object to set global state of the browser tab!

```js
window.myVarName = true;
```

This won't disappear when the function ends!

---

# **Where do we go from here?**